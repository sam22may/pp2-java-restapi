package com.restapi.TheBeanTalk.user;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Service
public class UserService {

    public List<User> getUsers(){

        return List.of(
                new User(
                        1L,
                        "sam123",
                        "Samuel",
                        "test@tes.com"
                )
        );
    }
}
