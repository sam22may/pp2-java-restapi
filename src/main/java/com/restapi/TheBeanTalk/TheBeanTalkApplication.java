package com.restapi.TheBeanTalk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheBeanTalkApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheBeanTalkApplication.class, args);
	}

}
